import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {

  public static void main(String[] args) {
    boolean flag = true;

    while (flag) {

      System.out.println("|   MENU                |");
      System.out.println("| Opções:               |");
      System.out.println("|        1. Opção 1     |");
      System.out.println("|        2. Opção 2     |");
      System.out.println("|        3. Sair        |");

      Scanner menu = new Scanner(System.in);
      int opcao = 0;
      System.out.println(" Selecione uma opção: ");
      try {
        opcao = menu.nextInt();

      } catch (InputMismatchException e) {
        menu.close();
        System.exit(0);
      }

      switch (opcao) {
        case 1:
          System.out.println("Você escolheu a primeira opção");
          break;
        case 2:
          System.out.println("Você escolheu a segunda opção");
          break;
        case 3:
          flag = false;
          System.out.println("O programa foi encerrado");
          break;
        default:
          System.out.println("Seleção inválida");
          break;
      }
    }

  }
}
